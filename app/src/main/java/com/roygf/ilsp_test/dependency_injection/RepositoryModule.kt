package com.roygf.ilsp_test.dependency_injection

import com.roygf.ilsp_test.data.repositories.NetworkRepository
import com.roygf.ilsp_test.domain.Repository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideNetworkRepository(networkRepository: NetworkRepository): Repository
}