package com.roygf.ilsp_test.presentation.primer_numbers.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.roygf.ilsp_test.domain.GetPrimeNumbersUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PrimeNumbersViewModel @Inject constructor(private val getPrimeNumbersUseCase: GetPrimeNumbersUseCase) :
    ViewModel() {

    val primeNumbers = MutableLiveData<String>()
    val errorMessage = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()

    fun checkValues(first: String, second: String) {

        if (first.isEmpty() || second.isEmpty()) {
            errorMessage.postValue("No puede haber campos vacíos")
            return
        }

        val firstNumber = first.toInt()
        val secondNumber = second.toInt()

        if (!validNumber(firstNumber) || !validNumber(secondNumber)) {
            errorMessage.postValue("Los numeros deben estar entre 0 y 200")
            return
        }

        if (firstNumber < secondNumber) {
            getPrimeNumbers(firstNumber, secondNumber)
        } else {
            errorMessage.postValue("Los números son incorrectos")
        }
    }

    private fun getPrimeNumbers(first: Int, second: Int) {
        viewModelScope.launch {
            loading.postValue(true)

            val numbersResult = getPrimeNumbersUseCase(first, second)
            if (!numbersResult.isNullOrEmpty()) {

                primeNumbers.postValue(getPrimeNumbersString(numbersResult))
            }
            loading.postValue(false)
        }
    }

    private fun validNumber(number: Int): Boolean {
        var valid = true
        if (number < 0 || number > 200) {
            valid = false
        }
        return valid
    }

    private fun getPrimeNumbersString(numbers: List<Int>): String {
        var text = ""
        for (number in numbers) {
            text += "$number "
        }
        return text
    }

}