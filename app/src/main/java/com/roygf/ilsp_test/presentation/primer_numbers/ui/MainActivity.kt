package com.roygf.ilsp_test.presentation.primer_numbers.ui

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.roygf.ilsp_test.databinding.ActivityMainBinding
import com.roygf.ilsp_test.presentation.primer_numbers.view_model.PrimeNumbersViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: PrimeNumbersViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.loading.observe(this, {
            binding.progress.isVisible = it
        })

        viewModel.errorMessage.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

        viewModel.primeNumbers.observe(this, {
            binding.textResults.text = it
        })

        binding.buttonSearch.setOnClickListener {
            val firstValue = binding.editFirstNumber.text.toString()
            val secondValue = binding.editSecondNumber.text.toString()
            viewModel.checkValues(firstValue, secondValue)
        }

    }
}