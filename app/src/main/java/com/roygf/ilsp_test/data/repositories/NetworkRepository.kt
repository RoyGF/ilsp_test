package com.roygf.ilsp_test.data.repositories

import com.roygf.ilsp_test.data.network.ApiClient
import com.roygf.ilsp_test.domain.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class NetworkRepository @Inject constructor(private val apiClient: ApiClient): Repository {

    override suspend fun getPrimeNumbers(first: Int, second: Int): List<Int> {
        //val numbers: List<Int> = listOf(1, 2, 3, 4, 5, 6)
        //return numbers
        return withContext(Dispatchers.IO) {
            apiClient.getPrimeNumbers(first, second)
        }
    }

}