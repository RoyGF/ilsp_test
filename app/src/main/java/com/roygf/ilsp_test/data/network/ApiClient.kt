package com.roygf.ilsp_test.data.network

import retrofit2.http.GET
import retrofit2.http.Path

interface ApiClient {

    @GET("primes/{firstValue}/{secondValue}")
    suspend fun getPrimeNumbers(
        @Path("firstValue") first: Int,
        @Path("secondValue") second: Int
    ): List<Int>

}