package com.roygf.ilsp_test.domain

interface Repository {
    suspend fun getPrimeNumbers(first: Int, second: Int): List<Int>
}