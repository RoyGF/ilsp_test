package com.roygf.ilsp_test.domain

import javax.inject.Inject

class GetPrimeNumbersUseCase @Inject constructor(private val repository: Repository) {
    suspend operator fun invoke(first: Int, second: Int): List<Int> =
        repository.getPrimeNumbers(first, second)
}